package com.epam.view;

import com.epam.controller.ParserController;
import com.epam.controller.ParserControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private ParserController controller = new ParserControllerImpl();
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public MainView() {
        initMenu();
        methodRun = new LinkedHashMap<>();
        methodRun.put("gson", () -> controller.parseGson());
        methodRun.put("jackson", () -> controller.parseJackson());
        methodRun.put("exit", () -> log.info("Exit"));
    }

    public void runMenu() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder flagExit = new StringBuilder("");
        do {
            menu.values().forEach(log::info);
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.warn("Input correct option");
            }
        } while (!flagExit.toString().equals("exit"));
    }

    private void initMenu(){
        menu = new LinkedHashMap<>();
        menu.put("gson", "gson - Run gson parser");
        menu.put("jackson", "jackson - Run jackson parser");
        menu.put("exit", "exit - Exit");
    }
}


