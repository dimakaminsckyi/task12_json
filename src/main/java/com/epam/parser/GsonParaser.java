package com.epam.parser;

import com.epam.comparator.CandyComparator;
import com.epam.model.Candy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GsonParaser {

    private static Logger log = LogManager.getLogger(GsonParaser.class);
    private CandyComparator comparator;

    public GsonParaser() {
        comparator = new CandyComparator();
    }

    public List<Candy> parse(File jsonFile){
        Type listType = new TypeToken<ArrayList<Candy>>(){}.getType();
        List<Candy> candies = null;
        try {
            candies = new Gson().fromJson(new FileReader(jsonFile), listType);
            candies.sort(comparator);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return candies;
    }
}
