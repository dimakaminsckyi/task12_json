package com.epam.parser;

import com.epam.comparator.CandyComparator;
import com.epam.model.Candy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JacksonParser {
    private static Logger log = LogManager.getLogger(GsonParaser.class);
    private CandyComparator comparator;

    public JacksonParser() {
        comparator = new CandyComparator();
    }

    public List<Candy> parse(File jsonFile){
        ObjectMapper objectMapper = new ObjectMapper();
        List<Candy> candyList = null;
        try {
             candyList = objectMapper.readValue(jsonFile ,new TypeReference<List<Candy>>(){});
             candyList.sort(comparator);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return candyList;
    }
}
