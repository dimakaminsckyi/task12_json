package com.epam.controller;

import com.epam.parser.GsonParaser;
import com.epam.parser.JacksonParser;
import com.epam.parser.JsonValidate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class ParserControllerImpl implements ParserController {

    private static Logger log = LogManager.getLogger(ParserControllerImpl.class);
    private GsonParaser gsonParaser;
    private JacksonParser jacksonParser;
    private JsonValidate jsonValidate;
    private File jsonSchemaPath;
    private File jsonPath;

    public ParserControllerImpl() {
        jsonSchemaPath = new File("src\\main\\resources\\json\\candiesSchema.json");
        jsonPath = new File("src\\main\\resources\\json\\candies.json");
        jsonValidate = new JsonValidate();
        gsonParaser = new GsonParaser();
        jacksonParser = new JacksonParser();
    }

    @Override
    public void parseJackson() {
        if (jsonValidate.isValid(jsonPath,jsonSchemaPath)){
            jacksonParser.parse(jsonPath).forEach(log::info);
        }else{
            log.error("JSON is not valid");
        }
    }

    @Override
    public void parseGson() {
        if (jsonValidate.isValid(jsonPath, jsonSchemaPath)) {
            gsonParaser.parse(jsonPath).forEach(log::info);
        }else {
            log.error("JSON is not valid");
        }
    }
}
