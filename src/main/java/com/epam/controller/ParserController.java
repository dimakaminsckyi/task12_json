package com.epam.controller;

public interface ParserController {

    void parseJackson();

    void parseGson();
}
