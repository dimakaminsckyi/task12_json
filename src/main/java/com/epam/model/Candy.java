package com.epam.model;

import java.util.List;

public class Candy {

    private int candyId;
    private String name;
    private int energy;
    private String production;
    private Type type;
    private List<Ingredient> ingredients;
    private Value value;

    public int getEnergy() {
        return energy;
    }

    public void setCandyId(int candyId) {
        this.candyId = candyId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "candyId=" + candyId +
                ", name='" + name + '\'' +
                ", energy=" + energy +
                ", production='" + production + '\'' +
                ", type=" + type +
                ", ingredients=" + ingredients +
                ", value=" + value +
                "}\n";
    }
}
