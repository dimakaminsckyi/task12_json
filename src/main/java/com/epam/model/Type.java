package com.epam.model;

public class Type {

    private String typeName;
    private boolean filled;

    public void setTypeName(String type) {
        this.typeName = type;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "Type{" +
                "typeName='" + typeName + '\'' +
                ", filled=" + filled +
                '}';
    }
}
